import 'package:cripto_moeda/models/moeda.dart';
import 'package:cripto_moeda/repository/moeda_repository.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MoedasPage extends StatefulWidget {
  const MoedasPage({Key? key}) : super(key: key);

  @override
  _MoedasPageState createState() => _MoedasPageState();
}

class _MoedasPageState extends State<MoedasPage> {
  NumberFormat real = NumberFormat.currency(locale: "pt_BR", name: "R\$");
  final tabela = MoedaRepository.tabela;
  List<Moeda> selecionadas = [];

  appDinamic(){
    if(selecionadas.isEmpty){
      return AppBar(
        title: Text("Cripto Moeda"),
      );
    }else{
      return AppBar(
        leading: IconButton(
          onPressed: (){
            setState(() {
              selecionadas = [];
            });
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text("${selecionadas.length} foram selecionadas"),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appDinamic(),
        body: ListView.separated(
          itemBuilder: (BuildContext context, int moeda) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListTile(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                leading: (selecionadas.contains(tabela[moeda]))
                    ? const CircleAvatar(
                        child: Icon(
                          Icons.check,
                          color: Colors.green,
                        ),
                      )
                    : Image.asset(tabela[moeda].icone),
                title: Text(tabela[moeda].nome),
                subtitle: Text(tabela[moeda].sigla),
                trailing: Text(real.format(tabela[moeda].preco)),
                selected: selecionadas.contains(tabela[moeda]),
                selectedTileColor: Colors.indigo[50],
                onLongPress: () {
                  setState(() {
                    (selecionadas.contains(tabela[moeda]))
                        ? selecionadas.remove(tabela[moeda])
                        : selecionadas.add(tabela[moeda]);
                  });
                },
              ),
            );
          },
          separatorBuilder: (_, __) => const Divider(
            thickness: 3,
          ),
          itemCount: tabela.length,
        ));
  }
}
